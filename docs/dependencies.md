# Dependencies

This application makes use of the following Go packages:

* [`gojsonschema`](github.com/xeipuuv/gojsonschema): Validating user provided JSON resume against official schema
