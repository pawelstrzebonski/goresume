# TODOs

This project is by no means finished/polished. A the following is a shopping list of improvements (in no particular order):

* JSON resume file generation (TUI to guide user through resume generation?)
* More/better resume templates
* General code/project improvements (tests, CI, documentation, etc)
