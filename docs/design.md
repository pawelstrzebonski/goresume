# Design Document

## Code

The bulk of the application code is within the `main.go` file that handles user inputs, JSON resume validation, JSON resume parsing, as well as the application of various templates to generate resumes in various file formats (Markdown and HTML as of writing).

The `main_test.go` file contains basic unit tests for the functions implemented in the `main.go` source code file.

## External Assets

This project contains some external assets in the `external/` directory. Primarily, these are JSON files from the [JSON Resume Schema project](https://github.com/jsonresume/resume-schema). These are used as references for validating user provided resumes (`schema.json`) and a sample JSON resume file to test this project's functionality (`sample.resume.json`).

## Default Templates

The `templates/` directory contains the default resume templates that will be embedded into the compiled binary and used if the user does not specify and provide a template of their own. As of writing, two templates are included.

`resume.md.tmpl` is a relatively simple Markdown template that will be used by Go's standard `text/template` text templating library.

`resume.html.tmpl` is a relatively simple HTML template that will be used by Go's standard `html/template` HTML templating library.
