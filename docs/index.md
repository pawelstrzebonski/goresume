# GoResume

A [JSON Resume](https://jsonresume.org/) tool, written in Go.

Run `./goresume -h` for a list of all the arguments/options to this program.
