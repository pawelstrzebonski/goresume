{ pkgs ? import <nixpkgs> {} }:
with pkgs;

buildGoModule rec {
  name = "goresume";
  vendorHash = "sha256-K3rhuJcV9T7Aj5TEJJ/r2uCDA9SBDjpUz0v8j9HkFdw=";
  rev = "24b6e1f5b91142fdfbe6d58bbe84718f16d99a5e";
  src = fetchFromGitLab {
    inherit rev;
    owner = "pawelstrzebonski";
    repo = "goresume";
    sha256 = "sha256-2Cw/ByW0Lxs3+RViFaolPheolLrKWrQcb2hN2ychUhA=";
  };
  meta = with lib; {
    description = "A Go command line application for generating resume documents from a JSON Resume file.";
    homepage = "https://gitlab.com/pawelstrzebonski/goresume";
    license = licenses.agpl3Plus;
    #maintainers = with maintainers; [ pawelstrzebonski ];
    platforms = platforms.linux;
  };
}
