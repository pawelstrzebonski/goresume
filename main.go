package main

import (
	"bytes"
	"embed"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/xeipuuv/gojsonschema"
	htmltemplate "html/template"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	txttemplate "text/template"
)

//go:embed templates/* external/jsonresume/resume-schema/schema.json
var f embed.FS

func logfatalerror(err error) {
	if err != nil {
		log.Fatal("FATAL ERROR: ", err)
	}
}
func logerror(err error) {
	if err != nil {
		log.Print("ERROR: ", err)
	}
}

type Location struct {
	Address     string
	PostalCode  string
	City        string
	CountryCode string
	Region      string
}
type Profile struct {
	Network  string
	Username string
	Url      string
}
type Basics struct {
	Name     string
	Label    string
	Image    string
	Email    string
	Phone    string
	Url      string
	Summary  string
	Location Location
	Profiles []Profile
}
type Work struct {
	Name       string
	Position   string
	Url        string
	StartDate  string
	EndDate    string
	Summary    string
	Highlights []string
}
type Volunteer struct {
	Organization string
	Position     string
	Url          string
	StartDate    string
	EndDate      string
	Summary      string
	Highlights   []string
}
type Education struct {
	Institution string
	Url         string
	Area        string
	StudyType   string
	StartDate   string
	EndDate     string
	Score       string
	Courses     []string
}
type Awards struct {
	Title   string
	Data    string
	Awarder string
	Summary string
}
type Certificates struct {
	Name   string
	Data   string
	Issuer string
	Url    string
}
type Publications struct {
	Name        string
	Publisher   string
	ReleaseDate string
	Url         string
	Summary     string
}
type Skills struct {
	Name     string
	Level    string
	Keywords []string
}
type Languages struct {
	Language string
	Fluency  string
}
type Interests struct {
	Name     string
	Keywords []string
}
type References struct {
	Name      string
	Reference string
}
type Projects struct {
	Name        string
	Description string
	Highlights  []string
	Keywords    []string
	StartDate   string
	EndDate     string
	Url         string
	Roles       []string
	Entity      string
	Type        string
}
type Meta struct {
	Canonical    string
	Version      string
	LastModified string
}
type Resume struct {
	Basics       Basics
	Work         []Work
	Volunteer    []Volunteer
	Education    []Education
	Awards       []Awards
	Certificates []Certificates
	Publications []Publications
	Skills       []Skills
	Languages    []Languages
	Interests    []Interests
	References   []References
	Projects     []Projects
	Meta         Meta
}

func checkResume(filename string) bool {
	// Load JSON resume schema
	schema, err := f.ReadFile("external/jsonresume/resume-schema/schema.json")
	logfatalerror(err)
	schemaLoader := gojsonschema.NewStringLoader(string(schema))
	log.Println("Loaded schema")
	// Load the JSON resume
	dir, err := os.Getwd()
	logfatalerror(err)
	documentLoader := gojsonschema.NewReferenceLoader("file://" + filepath.Join(dir, filename))
	log.Println("Loaded resume")
	// Validate resume against schema
	result, err := gojsonschema.Validate(schemaLoader, documentLoader)
	logfatalerror(err)
	log.Println("Validated resume")
	// Check if resume is valid
	if result.Valid() {
		fmt.Println("The document is valid")
		return true
	} else {
		fmt.Println("The document is not valid. see errors :")
		// Iterate over schema non-compliance errors
		for _, desc := range result.Errors() {
			fmt.Println("- ", desc)
		}
		return false
	}
}

func loadResume(filename string) Resume {
	// Read the JSON resume file
	b, err := ioutil.ReadFile(filename)
	logfatalerror(err)
	// Parse JSON into Resume struct
	var resume Resume
	err = json.Unmarshal(b, &resume)
	logfatalerror(err)
	return resume
}

func (r *Resume) genMD(TEMPLATEMD *string) string {
	var tmpl *txttemplate.Template
	// Check provided template filename
	if len(*TEMPLATEMD) > 0 {
		// If a file was provided, load it
		tmpl0, err := txttemplate.ParseFiles(*TEMPLATEMD)
		logfatalerror(err)
		tmpl = tmpl0
	} else {
		// If no file provided, read the default template from embedded filesystem
		template, err := f.ReadFile("templates/resume.md.tmpl")
		logfatalerror(err)
		tmpl0, err := txttemplate.New("test").Parse(string(template))
		logfatalerror(err)
		tmpl = tmpl0
	}
	// Apply template to resume and read into a string
	var buff bytes.Buffer
	err := tmpl.Execute(&buff, r)
	logfatalerror(err)
	return buff.String()
}

func (r *Resume) genHTML(TEMPLATEHTML *string) string {
	var tmpl *htmltemplate.Template
	// Check provided template filename
	if len(*TEMPLATEHTML) > 0 {
		// If a file was provided, load it
		tmpl0, err := htmltemplate.ParseFiles(*TEMPLATEHTML)
		logfatalerror(err)
		tmpl = tmpl0
	} else {
		// If no file provided, read the default template from embedded filesystem
		template, err := f.ReadFile("templates/resume.html.tmpl")
		logfatalerror(err)
		tmpl0, err := htmltemplate.New("test").Parse(string(template))
		logfatalerror(err)
		tmpl = tmpl0
	}
	// Apply template to resume and read into a string
	var buff bytes.Buffer
	err := tmpl.Execute(&buff, r)
	logfatalerror(err)
	return buff.String()
}

func main() {
	// Get run options from flags
	FILENAME := flag.String("resume", "resume.json", "JSON resume file to process")
	MD := flag.Bool("md", false, "Create markdown file")
	HTML := flag.Bool("html", false, "Create HTML file")
	VERIFY := flag.Bool("verify", false, "Verify JSON file against schema")
	TEMPLATEHTML := flag.String("templatehtml", "", "Template to use for HTML")
	TEMPLATEMD := flag.String("templatemd", "", "Template to use for markdown")
	OUTNAME := flag.String("o", "resume", "Filename base (excluding extension) for output")
	flag.Parse()
	fakemain(FILENAME, MD, HTML, VERIFY, TEMPLATEHTML, TEMPLATEMD, OUTNAME)
}
func fakemain(FILENAME *string, MD *bool, HTML *bool, VERIFY *bool, TEMPLATEHTML *string, TEMPLATEMD *string, OUTNAME *string) {
	// Validate resume against schema
	if *VERIFY {
		checkResume(*FILENAME)
	}
	// Load/parse resume
	resume := loadResume(*FILENAME)
	// Export to markdown
	if *MD {
		mdstring := resume.genMD(TEMPLATEMD)
		ioutil.WriteFile(*OUTNAME+".md", []byte(mdstring), 0644)
	}
	// Export to HTML
	if *HTML {
		htmlstring := resume.genHTML(TEMPLATEHTML)
		ioutil.WriteFile(*OUTNAME+".html", []byte(htmlstring), 0644)
	}
}
