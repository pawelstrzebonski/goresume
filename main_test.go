package main

import (
	"testing"
)

func TestCheckExampleResume(t *testing.T) {
	// Check resume against schema
	res := checkResume("external/jsonresume/resume-schema/sample.resume.json")
	// We expect some schema issues to be found on the sample resume
	if res {
		t.Errorf("Sample resume had some incomplete elements and should error!")
	}
}

func TestExampleResumeMD(t *testing.T) {
	// Load/parse sample resume
	resume := loadResume("external/jsonresume/resume-schema/sample.resume.json")
	tmpl := ""
	mdstring := resume.genMD(&tmpl)
	// We expect some schema issues to be found on the sample resume
	if len(mdstring) <= 0 {
		t.Errorf("Expect markdown resume to be non-empty!")
	}
	tmpl = "templates/resume.md.tmpl"
	mdstring = resume.genMD(&tmpl)
	// We expect some schema issues to be found on the sample resume
	if len(mdstring) <= 0 {
		t.Errorf("Expect markdown resume to be non-empty!")
	}
}
func TestExampleResumeHTML(t *testing.T) {
	// Load/parse sample resume
	resume := loadResume("external/jsonresume/resume-schema/sample.resume.json")
	tmpl := ""
	mdstring := resume.genHTML(&tmpl)
	// We expect some schema issues to be found on the sample resume
	if len(mdstring) <= 0 {
		t.Errorf("Expect markdown resume to be non-empty!")
	}
	tmpl = "templates/resume.html.tmpl"
	mdstring = resume.genHTML(&tmpl)
	// We expect some schema issues to be found on the sample resume
	if len(mdstring) <= 0 {
		t.Errorf("Expect markdown resume to be non-empty!")
	}
}
func TestMainEverythingButPDF(t *testing.T) {
	FILENAME := "external/jsonresume/resume-schema/sample.resume.json"
	MD := true
	HTML := true
	VERIFY := true
	TEMPLATEHTML := ""
	TEMPLATEMD := ""
	OUTNAME := "resume"
	fakemain(&FILENAME, &MD, &HTML, &VERIFY, &TEMPLATEHTML, &TEMPLATEMD, &OUTNAME)
}
func TestMainPDF(t *testing.T) {
	FILENAME := "external/jsonresume/resume-schema/sample.resume.json"
	MD := false
	HTML := false
	VERIFY := false
	TEMPLATEHTML := ""
	TEMPLATEMD := ""
	OUTNAME := "resume"
	fakemain(&FILENAME, &MD, &HTML, &VERIFY, &TEMPLATEHTML, &TEMPLATEMD, &OUTNAME)
}
