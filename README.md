# GoResume

A [JSON Resume](https://jsonresume.org/) tool, written in Go.

## Features

* Validate JSON Resume against [official standard and schema](https://github.com/jsonresume/resume-schema)
* Generate Markdown (text) resumes
* Generate HTML resumes
* Generate resumes using built-in default templates or user-supplied templates

## Installation/Usage

Run `./goresume -h` for a list of all the arguments/options to this program.

If you have [Nix](https://nixos.org/) then you can use the included `shell.nix` to start a shell with installed dependencies for development, or else you can use the `default.nix` script to build/install a specific version of this application. Note that `default.nix` specifies the commit to be built, so it may need to be updated to build the latest version.

## Third-Party Resources

The `external/` directory contains third-party files.

The `external/jsonresume/resume-schema/` directory contains JSON files from [https://github.com/jsonresume/resume-schema](https://github.com/jsonresume/resume-schema) under an MIT license.
